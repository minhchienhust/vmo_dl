from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from cvxopt import matrix, solvers

np.random.seed(22)

means = [[2, 2], [4, 2]]
cov = [[.3, .2], [.2, .3]]
N = 10
X0 = np.random.multivariate_normal(means[0], cov, N) # class 1
X1 = np.random.multivariate_normal(means[1], cov, N) # class -1
print(X0.shape)
X = np.concatenate((X0.T, X1.T), axis=1)
print("X.shape: {}".format(X.shape))
y = np.concatenate((np.ones((1, N)), -1 * np.ones((1, N))), axis=1) # labels
print("y.shape: {}".format(y.shape))

# build K
V = np.concatenate((X0.T, -X1.T), axis=1)
K = matrix(V.T.dot(V)) # see definition of V, K near eq (8)

p = matrix(-np.ones((2 * N, 1))) # all-one vector
# build A, b, G, h

G = matrix(-np.eye(2*N))
h = matrix(np.zeros((2*N, 1)))
A = matrix(y)
b = matrix(np.zeros((1, 1)))
solvers.options["show_progress"] = False
sol = solvers.qp(K, p, G, h, A, b)

l = np.array(sol["x"])
# print("lambda = ")
# print(l.T)

# Ta nhận thấy rằng hầu hết các giá trị của lambda đều rất nhỏ, tới 10^−9 hoặc 10^−10 . 
# Đây chính là các giá trị bằng 0 nhưng vì sai số tính toán nên nó khác 0 một chút. Chỉ có 3 giá trị khác 0, ta dự đoán là sẽ có 3 điểm là support vectors. 
# Ta đi tìm support set S rồi tìm nghiệm của bài toán:

epsilon = 1e-6 # just a small number, greater than 1e-9
S = np.where(l > epsilon)[0]

VS = V[:, S]
XS = X[:, S]
yS = y[:, S]
lS = l[S]

# calculate w and b 
w = VS.dot(lS)
b = np.mean(yS.T - w.T.dot(XS))

print("w.shape = ", w.shape)
print("b = ", b)


# y_plot = np.dot(X.T, w) + b
# y_plot = y_plot.T

fig, ax = plt.subplots()

x1 = np.arange(-10, 10, 0.1)
y1 = -w[0, 0]/w[1, 0]*x1 - b/w[1, 0]
y2 = -w[0, 0]/w[1, 0]*x1 - (b-1)/w[1, 0]
y3 = -w[0, 0]/w[1, 0]*x1 - (b+1)/w[1, 0]
plt.plot(x1, y1, 'k', linewidth = 3)
plt.plot(x1, y2, 'k')
plt.plot(x1, y3, 'k')


y4 = 10*x1
plt.plot(x1, y1, 'k')
plt.fill_between(x1, y1, color='red', alpha=0.1)
plt.fill_between(x1, y1, y4, color = 'blue', alpha = .1)


plt.plot(X0[:, 0], X0[:, 1], "bs", markersize=8, alpha=.8)
plt.plot(X1[:, 0], X1[:, 1], "ro", markersize = 8, alpha = .8)
# # axis limits
plt.axis('equal')
plt.ylim(0, 3)
plt.xlim(2, 4)

# hide ticks
cur_axes = plt.gca()
cur_axes.axes.get_xaxis().set_ticks([])
cur_axes.axes.get_yaxis().set_ticks([])

# add circles around support vectors 
for m in S:
    circle = plt.Circle((X[0, m], X[1, m] ), 0.1, color='k', fill = False)
    ax.add_artist(circle)

plt.xlabel("$x_1$", fontsize=20)
plt.ylabel("$x_2$", fontsize=20)



plt.show()
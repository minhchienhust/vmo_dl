import numpy as np
import matplotlib.pyplot as plt

# SSR: Sum of squared residual

# function = f(x) = b0 + b1*x
def ssr_gradient(x, y, b):
    res = b[0] + b[1] * x - y
    return res.mean(), (res * x).mean() # .mean() is a method of np.ndarray

# def gradient_descent(gradient, x, y, start, learning_rate, n_iter, tolerance=1e-6):
#     vector = start
#     for i in range(n_iter):
#         diff = -learning_rate * np.array(gradient(x, y, vector))
#         if np.all(np.abs(diff) <= tolerance):
#             break
#         vector += diff
#     return vector

X = np.array([*range(5, 60, 10)])
Y = np.array([5, 20, 14, 32, 22, 38])
print(X, Y)

# start = [0.5, 0.5]
# learning_rate = 0.0008
# n_iterations = 1000000
# vector = gradient_descent(gradient=ssr_gradient, x=X, y=Y, start=start, learning_rate=learning_rate, n_iter=n_iterations)

# print(vector)
def gradient_descent(gradient, x, y, start, learning_rate=0.1, n_iter=50, tolerance = 1e-6, dtype="float"):
    # Checking if the gradient is callable
    if not callable(gradient):
        raise TypeError("'gradient' must be callable")
    
    # Setting up the data type for Numpy arrays
    dtype_ = np.dtype(dtype)
    
    # Converting x and y to Numpy arrays
    x, y = np.array(x, dtype=dtype_), np.array(x, dtype=dtype_)
    if x.shape[0] != y.shape[0]:
        raise ValueError("'x' and 'y' lengths do no match")
    
    # Initializing the values of the variables
    vector = np.aray(start, dtype=dtype_)
    
    # Setting up and checking the learning learning_rate
    learning_rate = np.array(learning_rate, dtype=dtype_)
    if np.any(learning_rate <= 0):
        raise ValueError("'learning_rate' must be > 0")
    
    # Setting up and checking the maximal number of iterations
    n_iter = np.array(n_iter, dtype=dtype_)
    if n_iter <= 0:
        raise ValueError("'n_iter' must be > 0")
    
    # Setting up and checking the tolerance
    tolerance = np.array(tolerance, dtype=dtype_)
    if np.any(tolerance <= 0):
        raise ValueError("'tolerance' must be > 0")
    
    # Performing the gradient descent loop
    for i in range(n_iter):
        # Recalculating the difference
        diff = -learning_rate * gradient(x, y, vector)
        # Checking if the absolute difference is small enough
        if np.all(np.abs(diff) <= tolerance):
            break
        # Updating the values of the variables
        vector += diff
    return vector if vector.shape else vector.items()

# Momentum in Stochastic Gradient Descent
def sgd(gradient, x, y, start, learn_rate=0.1, decay_rate=0.0, batch_size=1, n_iter=50, tolerance=1e-6, dtype="float64", random_state=None):
    # Checking if the gradient is callable
    if not callable(gradient):
        raise TypeError("'gradient' must be callable")
    
    # Setting up the data type for NumPy arrays
    dtype_ = np.dtype(dtype)
    
    # Converting x and y to NumPy arrays
    x, y = np.array(x, dtype=dtype_), np.array(y, dtype=dtype_)
    n_obs = x.shape[0]
    if n_obs != y.shape[0]:
        raise ValueError("'x' and 'y' lengths do not match")
    xy = np.c_[x.reshape(n_obs, -1), y.reshape(n_obs, 1)]
    
    # Initializing the random number generator
    seed = None if random_state is None else int(random_state)
    rgn = np.random.default_rng(seed=seed)
    
    # Initializing the values of the variables
    vector = np.array(start, dtype=dtype_)
    
    # Setting up and checking the learning rate
    learn_rate = np.array(learn_rate, dtype=dtype_)
    if np.any(learn_rate <= 0):
        raise ValueError("'learn_rate' must be greater than zero")
    
    # Setting up and checking the decay rate
    decay_rate = np.array(decay_rate, dtype=dtype_)
    if np.any(decay_rate < 0) or np.any(decay_rate > 1):
        raise ValueError("'decay_rate' must be between zero and one")
    
    # Setting up and checking the size of minibatches
    batch_size = int(batch_size)
    if not 0 < batch_size <= n_obs:
        raise ValueError("'batch_size' must be > 0 and <= number of observations ")
    
    # Setting up and checking the maximal number of iterations
    # Setting up and checking the tolerance
    tolerance = np.array(tolerance, dtype=dtype_)
    if np.any(tolerance <= 0):
        raise ValueError("'tolerance' must be greater than zero")
    
    # Setting the difference to zero for the first iteration
    diff = 0
    
    # Performing the gradient descent loop
    for i in range(n_iter):
        # Shuffle x and y
        rgn.shuffle(xy)
         
        # Performing minibatch moves
        for start in range(0, n_obs, batch_size):
            stop = start + batch_size
            x_batch, y_batch = xy[start:stop, :-1], xy[start:stop, -1:]
            
            # Recalculating the difference
            grad = np.array(gradient(x_batch, y_batch, vector), dtype=dtype_)
            # diff = -learn_rate * grad
            diff = decay_rate * diff - learn_rate * grad
            
            # Checking if the absolute difference is small enough
            if np.all(np.abs(diff) <= tolerance):
                    break
            # Updating the values of the variables
            vector += diff
    return vector if vector.shape else vector.items()


print(sgd(ssr_gradient, X, Y, start=[0.5, 0.5], learn_rate=0.0008, batch_size=3, n_iter=100_000, random_state=0))
        
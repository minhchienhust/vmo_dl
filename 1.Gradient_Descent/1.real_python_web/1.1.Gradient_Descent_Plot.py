from typing import Collection
import numpy as np
import argparse
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import var

function_ = lambda x: x ** 2
gradient_ = lambda x: 2 * x
function_ = lambda x: (x ** 4) - (5 * x ** 2) - (3 * x)
gradient_ = lambda x: (4 * x ** 3) - (10 * x) - 3


# function_ = lambda x : x - np.log(x)
# gradient_ = lambda x : x - 1 / (x)

def gradient_descent(gradient, start, learning_rate, n_iter, tolerance=1e-6):
    X_ = [start]
    vector = start
    count = 0
    for i in range(n_iter):
        count += 1
        diff = -learning_rate * gradient(vector)
        if np.all(np.abs(diff) <= tolerance):
            break
        vector += diff
        X_.append(vector)
    print("last value: {} after num_iterations {}".format(vector, count))
    print("diff: {}".format(diff))
    return vector, np.array(X_)


ap = argparse.ArgumentParser()
ap.add_argument("-n", "--n_interations", required=False, default=100)
ap.add_argument("-l", "--learning_rate", required=False, default=0.01)
args = vars(ap.parse_args())

x_start = 0
learning_rate = float(args["learning_rate"])
n_interations = int(args["n_interations"])
last_value, X_ = gradient_descent(gradient=gradient_, start=x_start, learning_rate=learning_rate, n_iter=n_interations)

x_value_1 = -3
x_value_2 = 3
X = np.linspace(x_value_1, x_value_2, 100)

Y = function_(X)
Y_ = function_(X_)

plt.plot(X, Y)
plt.title("{} interations & learning rate: {}".format(n_interations, learning_rate))
plt.plot(X_[0], Y_[0], "o", color="blue")
plt.text(X_[0], Y_[0], "start")
plt.text(X_[1], Y_[1], "1")
plt.text(X_[2], Y_[2], "2")
plt.text(X_[3], Y_[3], "3")
plt.plot(X_[1:-1], Y_[1:-1], "*")
plt.plot(X_[1:-1], Y_[1:-1])
plt.plot(X_[-1], Y_[-1], "o", color="black")
plt.text(X_[-1], Y_[-1], "stop")
plt.grid()
plt.show()

import pandas as pd
from scrapy.crawler import CrawlerProcess

from service.connect_postgres_pgadmin4 import upload_to_db
from service.csv_file_uk import spyder_crawl_csv_file

if __name__ == "__main__":

    # Import csv file in folder to postgresql db

    df = pd.read_csv("data/2.7/pcd11_par11_wd11_lad11_ew_lu.csv", delimiter=",", encoding="ISO-8859-1", header=0)
    upload_to_db(database_name="chien_test",
                 table_name="two_dot_seven",
                 file="data/2.7/pcd11_par11_wd11_lad11_ew_lu.csv",
                 df=df)

    # Crawl csv in web page
    crawlerProcess = CrawlerProcess()
    spyder_crawl_csv_file.crawl(crawlerProcess)
    crawlerProcess.start()

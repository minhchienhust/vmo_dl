import os
from sqlite3 import OperationalError

from sqlalchemy import create_engine

from service.connect_postgres_pgadmin4 import connect_to_posgres, split_sql_expressions

database_name = "vmo_demo"

engine = create_engine('postgresql://root:root@localhost:5432/' + database_name) # 'postgresql://user:password@localhost:5432/database_name
conn = connect_to_posgres(database_name=database_name)
cursor = conn.cursor()

path = "SQL"

list_sql_file = os.listdir(path)
list_sql_file.sort()



# print(*list_sql_file)
for i in list_sql_file[:1]:

    fd = open(os.path.join(path, i), 'r')
    sqlFile = fd.read()
    fd.close()

    # sqlCommands = sqlFile.split(';')
    # # cmt = sqlCommands[1][-1:]
    # # print(cmt)
    #
    # # cursor.execute(cmt)
    #
    #
    # # print(i)
    for command in split_sql_expressions(sqlFile):
        cursor.execute(command)
        # print(command)
        # This will skip and report errors
        # try:
        #     if len(command.split("\n--")) > 1 and command[-1] != "-":
        #         # print(command)
        #         continue
        #     print(command)
        #     cursor.execute(command)
        # except OperationalError as msg:
        #     print("Command skipped: ", msg)
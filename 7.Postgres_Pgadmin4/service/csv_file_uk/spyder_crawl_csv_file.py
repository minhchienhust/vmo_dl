import csv
import logging
import sys
import urllib

import numpy as np
import pandas as pd
import requests, zipfile, io
import scrapy
from pymongo import database
from scrapy.crawler import CrawlerProcess

# logging.getLogger('scrapy').setLevel(logging.WARNING)
from service.connect_postgres_pgadmin4 import upload_data_from_csv_to_sql_split, clean_colname, upload_to_db, \
    read_csv_encoding

# sys.path.insert(0, "../7.Postgres_Pgadmin4/source")
# from connect_postgres_pgadmin4 import create_table_from_csv_file # import library


class csvUkSpider(scrapy.Spider):
    name = "csv_uk"
    process = scrapy.crawler.CrawlerProcess(
        {
            "USER_AGENT": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 "
                          "(KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
        }
    )

    def start_requests(self):
        urls = [
            "https://data.gov.uk/dataset/cda6d1fa-52da-47c3-b23a-6cee1e6034b1/lau2-to-lau1-to-nuts3-to-nuts2-to-nuts1-january-2018-lookup-in-united-kingdom-v3",
            "https://www.doogal.co.uk/PostcodeDownloads.php",
            "https://data.gov.uk/dataset/86beb640-9fa4-4131-b330-fc26d74c074f/local-authority-district-december-2018-to-nuts3-to-nuts2-to-nuts1-january-2018-lookup-in-united-kingdom",
            "https://geoportal.statistics.gov.uk/datasets/6a46e14a6c2441e3ab08c7b277335558/about",
            "https://geoportal.statistics.gov.uk/datasets/82889274464b48ae8bf3e9458588a64b",
            "",
        ]

        yield scrapy.Request(urls[0], callback=self.parse_1)
        # yield scrapy.Request(urls[1], callback=self.parse_2)
        # yield scrapy.Request(urls[2], callback=self.parse_3)
        # yield scrapy.Request(urls[3], callback=self.parse_4)
        # yield scrapy.Request(urls[4], callback=self.parse_5)

    def parse_1(self, response):
        csv_link_lau_href = response.css(".govuk-table__row~ .govuk-table__row+ .govuk-table__row .govuk-table__cell:nth-child(1) .govuk-link")
        csv_link_lau_href = csv_link_lau_href.css("::attr(href)").extract()[0]
        print("1\n1\n1\n{}\n1\n1\n1".format(csv_link_lau_href))
        csv_read = urllib.request.urlopen(csv_link_lau_href)

        df = read_csv_encoding(csv_read)

        upload_to_db(database_name="chien_test", table_name="lau2_lau1_nuts", file="lau2_lau1_nuts.csv", df=df)
        # create_table_from_csv_file(database_name="vmo_demo", table_name="lau2_lau1_nuts", csv_file=csv_read)

    def parse_2(self, response):
        csv_postcode_href = response.css("p+ a:nth-child(3)").css("::attr(href)").extract()[0]
        csv_postcode_href = "https://www.doogal.co.uk/" + csv_postcode_href

        print("2\n2\n2\n{}\n2\n2\n2".format(csv_postcode_href))

        r = requests.get(csv_postcode_href)
        z = zipfile.ZipFile(io.BytesIO(r.content), "r")
        csv_read = z.open("postcodes.csv")


        upload_data_from_csv_to_sql_split(csv_read, database_name="vmo_demo", table_name="postcode", chunk_size=100000, encoding="ISO-8859-1")
        # df = pd.read_csv(csv_read, delimiter=",", encoding="ISO-8859-1", header=0)
        # upload_to_db(database_name="vmo_demo", table_name="postcode", file="postcode.csv", df=df)
        # create_table_from_csv_file(database_name="vmo_demo", table_name="postcode", csv_file=csv_read)

    def parse_3(self, response):
        csv_local_authority_href = response.css(".govuk-table__row:nth-child(4) .govuk-table__cell:nth-child(1) .govuk-link")
        csv_local_authority_href = csv_local_authority_href.css("::attr(href)").extract()[0]
        print("3\n3\n3\n{}\n3\n3\n3".format(csv_local_authority_href))
        csv_read = urllib.request.urlopen(csv_local_authority_href)

        df = read_csv_encoding(csv_read)
        upload_to_db(database_name="vmo_demo", table_name="local_authority", file="local_authority.csv", df=df)

    def parse_4(self, response):
        csv_local_msoa_lsoa = "https://www.arcgis.com/sharing/rest/content/items/6a46e14a6c2441e3ab08c7b277335558/data"
        print("4\n4\n4\n{}\n4\n4\n4".format(csv_local_msoa_lsoa))
        r = requests.get(csv_local_msoa_lsoa)
        z = zipfile.ZipFile(io.BytesIO(r.content), "r")
        csv_read = z.open("PCD_OA_LSOA_MSOA_LAD_FEB20_UK_LU.csv")

        df = pd.read_csv(csv_read, delimiter=",", encoding="ISO-8859-1", header=0)

        upload_to_db(database_name="vmo_demo", table_name="msoa_lsoa", file="msoa_lsoa.csv", df=df)

        # push_data_from_csv_to_sql_split(df, database_name="vmo_demo")
        # create_table_from_csv_file(database_name="vmo_demo", table_name="msoa_lsoa", csv_file=csv_read)

    def parse_5(self, response):
        csv_local_ons_postcode = "https://www.arcgis.com/sharing/rest/content/items/82889274464b48ae8bf3e9458588a64b/data"
        print("5\n5\n5\n{}\n5\n5\n5".format(csv_local_ons_postcode))
        r = requests.get(csv_local_ons_postcode)
        z = zipfile.ZipFile(io.BytesIO(r.content), "r")
        csv_read = z.open("Data/ONSPD_FEB_2020_UK.csv")

        df = pd.read_csv(csv_read, delimiter=",", encoding="ISO-8859-1", header=0)

        upload_to_db(database_name="vmo_demo", table_name="ons_postcode", file="ons_postcode.csv", df=df)


def crawl(process: CrawlerProcess):
    process_ = process.crawl(csvUkSpider)

import sqlite3
from sqlite3 import OperationalError

conn = sqlite3.connect('Client_DB.db')
c = conn.cursor()


def execute_sqlfile(filename):
    c.execute("CREATE TABLE clients_parameters (adress text, ie text)")
    #
    fd = open(filename, 'r')
    sqlFile = fd.readlines()
    fd.close()
    lvalues = [tuple(v.split(';')) for v in sqlFile[1:]]
    try:
        # print(command)
        c.executemany("INSERT INTO clients_parameters VALUES (?, ?)", lvalues)
    except OperationalError as msg:
        print("Command skipped: ", msg)


execute_sqlfile('clients.sql')

print(c.rowcount)

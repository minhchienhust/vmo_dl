from scrapy.crawler import CrawlerProcess

from csv_file_uk import spyder_crawl_csv_file

if __name__ == "__main__":
    crawlerProcess = CrawlerProcess()
    spyder_crawl_csv_file.crawl(crawlerProcess)
    crawlerProcess.start()

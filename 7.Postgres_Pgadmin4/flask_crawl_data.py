"""
File này sẽ chạy API để crawl dữ liệu từ ONS UK về
Khi truy cập trang web, bấm vào nút start crawl sẽ bắt đầu crawl.
    Crawl xong, web sẽ hiển thị ra thời gian thực hiện xong lệnh crawl
"""
# -------------------------------------------------------------------------------------
# import crochet
import pandas as pd

from service.connect_postgres_pgadmin4 import upload_to_db
import time

from service.csv_file_uk.spyder_crawl_csv_file import csvUkSpider

# crochet.setup()

from flask import Flask , render_template
from scrapy.crawler import CrawlerRunner
import time

# Importing our Scraping Function from the amazon_scraping file

# Creating Flask App variable

app = Flask(__name__, template_folder="service/templates")


crawl_runner = CrawlerRunner()

# By Deafult Flask will come into this when we run the file
@app.route('/')
def index():
    return render_template('index_1.html', data='hey') # Returns index_1.html file in templates folder

@app.route("/crawl", methods=["POST"])
def crawl():
    start_time = time.time()
    df = pd.read_csv("data/2.7/pcd11_par11_wd11_lad11_ew_lu.csv", delimiter=",", encoding="ISO-8859-1", header=0)
    upload_to_db(database_name="chien_test",
                 table_name="two_dot_seven",
                 file="data/2.7/pcd11_par11_wd11_lad11_ew_lu.csv",
                 df=df)

    # Crawl csv in web page

    crawl_runner = CrawlerRunner()
    crawl_runner.crawl(csvUkSpider)

    #
    print("crawling")
    # crawlerProcess.stop()
    print("stop")
    stop_time  = time.time()
    return  render_template("crawl.html", data = "Run in {} second".format(round(stop_time - start_time), 2))
if __name__ == "__main__":
    app.run(port=5220 , debug=True)



/*
File này tiến hành Normalize Database
1. Tạo foreign key cho các bảng to
2. Xóa các data không liên quan đi
 */


-- B1: Tạo khóa chính của bảng cần normalize
------------------------------------------ #### -----------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.public.local_authority ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.public.msoa_lsoa ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.public.postcode ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.public.ons_postcode ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.public.two_dot_seven ADD COLUMN id SERIAL PRIMARY KEY;

--
-- B2: Tạo khóa phụ của bảng cần tham chiếu tới bảng REFERENCE --- Rồi --- adding foreign key --- Rồi --- gán cho thành FOREIGN KEY
-- ////////////////////////////////////////lau2_lau1_nuts//////////////////////////////////////////////////////////////////////////
------------------------------------------Table REFERENCE lau1_id-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts ADD COLUMN lau1_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.lau2_lau1_nuts lln
-- SET lau1_id = c_lau1_table.id
-- FROM vmo_demo_test.create_table_from_public.lau1_table AS c_lau1_table
-- WHERE lln.lau117cd = c_lau1_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts
--     ADD CONSTRAINT fk_lau1_lau2_lau1_nuts
--     FOREIGN KEY (lau1_id)
--     REFERENCES vmo_demo_test.create_table_from_public.lau1_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi

--
------------------------------------------Table REFERENCE lau1_id-----------------------------------------------------------------
--
-- Talbe lau2_id
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts ADD COLUMN lau2_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.lau2_lau1_nuts lln
-- SET lau2_id = c_lau2_table.id
-- FROM vmo_demo_test.create_table_from_public.lau2_table AS c_lau2_table
-- WHERE lln.lau217cd = c_lau2_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts
--     ADD CONSTRAINT fk_lau2_lau2_lau1_nuts
--     FOREIGN KEY (lau2_id)
--     REFERENCES vmo_demo_test.create_table_from_public.lau2_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE nut1_id-----------------------------------------------------------------

-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts ADD COLUMN nut1_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.lau2_lau1_nuts lln
-- SET nut1_id = c_nut1_table.id
-- FROM vmo_demo_test.create_table_from_public.nut1_table AS c_nut1_table
-- WHERE lln.nuts118cd = c_nut1_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts
--     ADD CONSTRAINT fk_nut1_lau2_lau1_nuts
--     FOREIGN KEY (nut1_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut1_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE nut2_id-----------------------------------------------------------------

-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts ADD COLUMN nut2_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.lau2_lau1_nuts lln
-- SET nut2_id = c_nut2_table.id
-- FROM vmo_demo_test.create_table_from_public.nut2_table AS c_nut2_table
-- WHERE lln.nuts218cd = c_nut2_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts
--     ADD CONSTRAINT fk_nut2_lau2_lau1_nuts
--     FOREIGN KEY (nut2_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut2_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE nut3_id-----------------------------------------------------------------

-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts ADD COLUMN nut3_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.lau2_lau1_nuts lln
-- SET nut3_id = c_nut3_table.id
-- FROM vmo_demo_test.create_table_from_public.nut3_table AS c_nut3_table
-- WHERE lln.nuts318cd = c_nut3_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts
--     ADD CONSTRAINT fk_nut3_lau2_lau1_nuts
--     FOREIGN KEY (nut3_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut3_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi


-- ////////////////////////////////////////local_authority//////////////////////////////////////////////////////////////////////////

------------------------------------------Table REFERENCE ladcd-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.local_authority ADD COLUMN ladcd_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.local_authority la
-- SET ladcd_id = c_ladcd_table.id
-- FROM vmo_demo_test.create_table_from_public.ladcd_table AS c_ladcd_table
-- WHERE la.lad18cd = c_ladcd_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.local_authority
--     ADD CONSTRAINT fk_ladcd_local_authority
--     FOREIGN KEY (ladcd_id)
--     REFERENCES vmo_demo_test.create_table_from_public.ladcd_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE lau1_id-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.local_authority ADD COLUMN lau1_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.local_authority la
-- SET lau1_id = c_lau1_table.id
-- FROM vmo_demo_test.create_table_from_public.lau1_table AS c_lau1_table
-- WHERE la.lau118cd = c_lau1_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.local_authority
--     ADD CONSTRAINT fk_lau1_local_authority
--     FOREIGN KEY (lau1_id)
--     REFERENCES vmo_demo_test.create_table_from_public.lau1_table(id);
-- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE nut1_id-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.local_authority ADD COLUMN nut1_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.local_authority la
-- SET nut1_id = c_nut1_table.id
-- FROM vmo_demo_test.create_table_from_public.nut1_table AS c_nut1_table
-- WHERE la.nuts118cd = c_nut1_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.local_authority
--     ADD CONSTRAINT fk_nut1_local_authority
--     FOREIGN KEY (nut1_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut1_table(id);
-- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE nut2_id-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.local_authority ADD COLUMN nut2_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.local_authority la
-- SET nut2_id = c_nut2_table.id
-- FROM vmo_demo_test.create_table_from_public.nut2_table AS c_nut2_table
-- WHERE la.nuts218cd = c_nut2_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.local_authority
--     ADD CONSTRAINT fk_nut2_local_authority
--     FOREIGN KEY (nut2_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut2_table(id);
-- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE nut3_id-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.local_authority ADD COLUMN nut3_id INTEGER;
-- -- adding foreign key
-- UPDATE vmo_demo_test.public.local_authority la
-- SET nut3_id = c_nut3_table.id
-- FROM vmo_demo_test.create_table_from_public.nut3_table AS c_nut3_table
-- WHERE la.nuts318cd = c_nut3_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.local_authority
--     ADD CONSTRAINT fk_nut3_local_authority
--     FOREIGN KEY (nut3_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut3_table(id);
-- Sau đó xóa cột dư thừa ở bảng gốc đi




-- ////////////////////////////////////////msoa_lsoa//////////////////////////////////////////////////////////////////////////
------------------------------------------Table REFERENCE district_code-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.msoa_lsoa ADD COLUMN district_code_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.msoa_lsoa ml
-- SET district_code_id = c_district_code_table.id
-- FROM vmo_demo_test.create_table_from_public.district_code_table AS c_district_code_table
-- WHERE SUBSTR(ml.pcds, 1, POSITION(' ' IN ml.pcds)) = c_district_code_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.msoa_lsoa
--     ADD CONSTRAINT fk_district_code_msoa_lsoa
--     FOREIGN KEY (district_code_id)
--     REFERENCES vmo_demo_test.create_table_from_public.district_code_table(id);


------------------------------------------Table REFERENCE ladcd-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.msoa_lsoa ADD COLUMN ladcd_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.msoa_lsoa ml
-- SET ladcd_id = c_ladcd_table.id
-- FROM vmo_demo_test.create_table_from_public.ladcd_table AS c_ladcd_table
-- WHERE ml.ladcd = c_ladcd_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.msoa_lsoa
--     ADD CONSTRAINT fk_ladcd_msoa_lsoa
--     FOREIGN KEY (ladcd_id)
--     REFERENCES vmo_demo_test.create_table_from_public.ladcd_table(id);



-- ////////////////////////////////////////ons_postcode//////////////////////////////////////////////////////////////////////////
------------------------------------------Table REFERENCE district_code-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.ons_postcode ADD COLUMN district_code_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.ons_postcode onp
-- SET district_code_id = c_district_code_table.id
-- FROM vmo_demo_test.create_table_from_public.district_code_table AS c_district_code_table
-- WHERE SUBSTR(onp.pcds, 1, POSITION(' ' IN onp.pcds)) = c_district_code_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.ons_postcode
--     ADD CONSTRAINT fk_district_code_ons_postcode
--     FOREIGN KEY (district_code_id)
--     REFERENCES vmo_demo_test.create_table_from_public.district_code_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi

------------------------------------------Table REFERENCE pcon-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.ons_postcode ADD COLUMN pcon_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.ons_postcode onp
-- SET pcon_id = c_pcon_table.id
-- FROM vmo_demo_test.create_table_from_public.pcon_table AS c_pcon_table
-- WHERE onp.pcon = c_pcon_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.ons_postcode
--     ADD CONSTRAINT fk_pcon_ons_postcode
--     FOREIGN KEY (pcon_id)
--     REFERENCES vmo_demo_test.create_table_from_public.pcon_table(id);

-- ////////////////////////////////////////postcode//////////////////////////////////////////////////////////////////////////
------------------------------------------Table REFERENCE district_code-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.postcode ADD COLUMN district_code_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.postcode pc
-- SET district_code_id = c_district_code_table.id
-- FROM vmo_demo_test.create_table_from_public.district_code_table AS c_district_code_table
-- WHERE SUBSTR(pc.pcds, 1, POSITION(' ' IN pc.pcds)) = c_district_code_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.postcode
--     ADD CONSTRAINT fk_district_code_postcode
--     FOREIGN KEY (district_code_id)
--     REFERENCES vmo_demo_test.create_table_from_public.district_code_table(id);

------------------------------------------Table REFERENCE pcon-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.postcode ADD COLUMN pcon_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.postcode pc
-- SET pcon_id = c_pcon_table.id
-- FROM vmo_demo_test.create_table_from_public.pcon_table AS c_pcon_table
-- WHERE pc.pcon = c_pcon_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.postcode
--     ADD CONSTRAINT fk_pcon_postcode
--     FOREIGN KEY (pcon_id)
--     REFERENCES vmo_demo_test.create_table_from_public.pcon_table(id);

-- ////////////////////////////////////////two_dot_seven//////////////////////////////////////////////////////////////////////////
------------------------------------------Table REFERENCE district_code-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.two_dot_seven ADD COLUMN district_code_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.two_dot_seven tds
-- SET district_code_id = c_district_code_table.id
-- FROM vmo_demo_test.create_table_from_public.district_code_table AS c_district_code_table
-- WHERE SUBSTR(tds.pcds, 1, POSITION(' ' IN tds.pcds)) = c_district_code_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.two_dot_seven
--     ADD CONSTRAINT fk_district_code_two_dot_seven
--     FOREIGN KEY (district_code_id)
--     REFERENCES vmo_demo_test.create_table_from_public.district_code_table(id);
-- -- Sau đó xóa cột dư thừa ở bảng gốc đi


------------------------------------------Table REFERENCE ladcd-----------------------------------------------------------------
-- ALTER TABLE vmo_demo_test.public.two_dot_seven ADD COLUMN ladcd_id INTEGER;
-- -- -- adding foreign key
-- UPDATE vmo_demo_test.public.two_dot_seven tds
-- SET ladcd_id = c_ladcd_table.id
-- FROM vmo_demo_test.create_table_from_public.ladcd_table AS c_ladcd_table
-- WHERE tds.lad11cd = c_ladcd_table.code;
-- -- gán cho thành FOREIGN KEY
-- ALTER TABLE vmo_demo_test.public.two_dot_seven
--     ADD CONSTRAINT fk_ladcd_two_dot_seven
--     FOREIGN KEY (ladcd_id)
--     REFERENCES vmo_demo_test.create_table_from_public.ladcd_table(id);
-- Sau đó xóa cột dư thừa ở bảng gốc đi






-- ////////////////////////////////////////Delete columns//////////////////////////////////////////////////////////////////////////

-- ALTER TABLE vmo_demo_test.public.lau2_lau1_nuts
-- DROP COLUMN fid;
-- DROP COLUMN lau117cd,
-- DROP COLUMN lau117nm,
-- DROP COLUMN lau217cd,
-- DROP COLUMN lau217nm,
-- DROP COLUMN nuts118cd,
-- DROP COLUMN nuts118nm,
-- DROP COLUMN nuts218cd,
-- DROP COLUMN nuts218nm,
-- DROP COLUMN nuts318cd,
-- DROP COLUMN nuts318nm;




-- ALTER TABLE vmo_demo_test.public.local_authority
-- DROP COLUMN fid;
-- DROP COLUMN lau118cd,
-- DROP COLUMN lau118nm;
-- DROP COLUMN nuts118cd,
-- DROP COLUMN nuts118nm,
-- DROP COLUMN nuts218cd,
-- DROP COLUMN nuts218nm,
-- DROP COLUMN nuts318cd,
-- DROP COLUMN nuts318nm,
-- DROP COLUMN lad18cd,
-- DROP COLUMN lad18nm;


-- ALTER TABLE vmo_demo_test.public.msoa_lsoa
-- DROP COLUMN pcds,
-- DROP COLUMN ladcd,
-- DROP COLUMN ladnm;

-- ALTER TABLE vmo_demo_test.public.ons_postcode
-- DROP COLUMN pcds;
--
-- ALTER TABLE vmo_demo_test.public.postcode
-- DROP COLUMN pcds;
--
-- ALTER TABLE vmo_demo_test.public.two_dot_seven
-- DROP COLUMN lad11cd,
-- DROP COLUMN lad11nm,
-- DROP COLUMN pcds;
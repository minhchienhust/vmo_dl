"""
File này sẽ run tất cả các query trong các file trong thư mục SQL
Khi truy cập trang web, bấm vào nút start run sql file sẽ bắt đầu thực hiện query các câu lệnh trong các file.
    Query xong, web sẽ hiển thị ra thời gian thực hiện xong lệnh query
"""
# -------------------------------------------------------------------------------------
# import crochet
import os
import time

from flask import Flask, render_template
from sqlalchemy import create_engine

from service.connect_postgres_pgadmin4 import connect_to_posgres
from service.connect_postgres_pgadmin4 import split_sql_expressions

# crochet.setup()

# Importing our Scraping Function from the amazon_scraping file

# Creating Flask App variable

app = Flask(__name__, template_folder="service/templates")


# By Deafult Flask will come into this when we run the file
def run_sql_file(folder_sql_file: str):
    database_name = "vmo_demo_test"

    engine = create_engine(
        'postgresql://root:root@localhost:5432/' + database_name)  # 'postgresql://user:password@localhost:5432/database_name
    conn = connect_to_posgres(database_name=database_name)
    cursor = conn.cursor()

    list_sql_file = os.listdir(folder_sql_file)
    list_sql_file.sort()
    for i in list_sql_file:
        fd = open(os.path.join(folder_sql_file, i), 'r')

        # Execute every command from the input file
        for command in split_sql_expressions(fd.read()):
            cursor.execute(command)


@app.route('/')
def index():
    return render_template('index_sql.html', data='hey')  # Returns index_1.html file in templates folder


@app.route("/sql", methods=["POST"])
def sql():
    start_time = time.time()

    run_sql_file(folder_sql_file="SQL")

    stop_time = time.time()
    return render_template("crawl.html", data="Run in {} second".format(round(stop_time - start_time), 2))


if __name__ == "__main__":
    app.run(port=5222, debug=True)

# Reference:
# https://stackoverflow.com/questions/38856534/execute-sql-file-with-multiple-statements-separated-by-using-pyodbc

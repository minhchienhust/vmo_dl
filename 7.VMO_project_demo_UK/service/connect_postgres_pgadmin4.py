import os
import time
from functools import wraps

import pandas as pd
import psycopg2
import sqlalchemy
from memory_profiler import memory_usage
from sqlalchemy import create_engine


def profile(fn):
    @wraps(fn)
    def inner(*args, **kwargs):
        fn_kwargs_str = ', '.join(f'{k}={v}' for k, v in kwargs.items())
        print(f'\n{fn.__name__}({fn_kwargs_str})')

        # Measure time
        t = time.perf_counter()
        retval = fn(*args, **kwargs)
        elapsed = time.perf_counter() - t
        print(f'Time   {elapsed:0.4}')

        # Measure memory
        mem, retval = memory_usage((fn, args, kwargs), retval=True, timeout=200, interval=1e-7)

        print(f'Memory {max(mem) - min(mem)}')
        return retval

    return inner


def connect_to_posgres(database_name):
    conn = psycopg2.connect(
        host="localhost",
        database=database_name,
        user="root",
        password="root")
    print("connect successfully")
    return conn


def code_postgresql_by_python(database_name: str, comment: str):
    conn = connect_to_posgres(database_name)
    conn.execute(comment)
    return conn


def create_table_from_csv_file(database_name: str, table_name: str, df):
    engine = create_engine(
        'postgresql://root:root@localhost:5432/' + database_name)
    # 'postgresql://user:password@localhost:5432/database_name
    # df = pd.read_csv(path_csv_file) Nếu dùng lệnh này thì cần khai báo path_csv_file ở trong laptop
    df.columns = [c.lower() for c in df.columns]  # postgres doesn't like capitals or spaces

    # logging.basicConfig()
    # logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

    df.to_sql(table_name,
              engine,
              if_exists="replace",  # options are ‘fail’, ‘replace’, ‘append’, default ‘fail’
              index=False,  # Do not output the index of the dataframe
              dtype={'col1': sqlalchemy.types.NUMERIC,
                     'col2': sqlalchemy.types.String})  # Datatypes should be [sqlalchemy types][1]


def read_csv_encoding(csv_read):
    try:
        df = pd.read_csv(csv_read, delimiter=",", encoding="utf-8", header=0)
    except:  # UnicodeDecodeError:
        df = pd.read_csv(csv_read, delimiter=",", encoding="ISO-8859-1", header=0)  # if utf-8 encoding error
    return df


def clean_string(string: str):
    special_char = '@!#$%^&*()<>?/\|}{~:;[]'
    for i in special_char:
        string = string.replace(i, '')
    return string


def clean_colname(df):
    # force column names to be lower case, no spaces, no dashes

    df.columns = [
        clean_string(
            x.lower().replace(" ", "_").replace("-", "_").replace(r"/", "_").replace("\\", "_").replace(".", "_")) for x
        in df.columns]
    print(df.columns)
    # processing data
    replacements = {
        'timedelta64[ns]': 'varchar',
        'object': 'varchar',
        'float64': 'float',
        'int64': 'int',
        'datetime64': 'timestamp'
    }
    col_str = ", ".join(
        "{} {}".format(n, d) for (n, d) in zip(df.columns, df.dtypes.replace(replacements)))

    return col_str, df.columns


def upload_data_from_csv_to_sql_split(csv_read, database_name: str, table_name: str, chunk_size: int, encoding: str):
    engine = create_engine(
        'postgresql://root:root@localhost:5432/' + database_name)
    # 'postgresql://user:password@localhost:5432/database_name
    conn = connect_to_posgres(database_name=database_name)
    cursor = conn.cursor()
    try:
        cursor.execute("drop table if exists %s;" % (table_name))
        print("done drop table")
    except:
        print("No action")
    batch_no = 1
    for df in pd.read_csv(csv_read, delimiter=",", encoding=encoding, chunksize=chunk_size, iterator=True):
        col_str, df.columns = clean_colname(df)

        df.to_sql(table_name,
                  engine,
                  if_exists="append")  # ,  # options are ‘fail’, ‘replace’, ‘append’, default ‘fail’
        # index=False#,  # Do not output the index of the dataframe
        # dtype={'col1': sqlalchemy.types.NUMERIC,
        #        'col2': sqlalchemy.types.String})  # Datatypes should be [sqlalchemy types][1]
        batch_no += 1
        print("index: {}".format(batch_no))


def drop_create_new_table(cursor, table_name: str, col_str):
    # drop table with same name
    cursor.execute("drop table if exists %s;" % (table_name))

    # create table
    cursor.execute("create table %s (%s);" % (table_name, col_str))
    print('{0} was created successfully'.format(table_name))


# @profile
def upload_to_db(database_name: str, table_name: str, file: str, df):
    conn = connect_to_posgres(database_name=database_name)
    cursor = conn.cursor()
    print('opened database successfully')

    col_str, dataframe_columns = clean_colname(df)
    print(col_str)

    # save df to csv
    df.to_csv(file, header=dataframe_columns, index=False, encoding='utf-8')

    # open the csv file, save it as an object
    my_file = open(file)
    print('file opened in memory')

    drop_create_new_table(cursor, table_name, col_str)

    # upload to db
    SQL_STATEMENT = """
    COPY %s FROM STDIN WITH
        CSV
        HEADER
        DELIMITER AS ','
    """

    cursor.copy_expert(sql=SQL_STATEMENT % table_name, file=my_file)
    print('file copied to db')

    cursor.execute("grant select on table %s to public" % table_name)
    conn.commit()
    cursor.close()

    # remove file after import to postgresql
    if file != "data/2.7/pcd11_par11_wd11_lad11_ew_lu.csv":
        if os.path.exists(file):
            os.remove(file)
        else:
            print("The file does not exist")

    print('table {0} imported to db completed'.format(table_name))

    # return


def split_sql_expressions(text):
    current = ''
    state = None
    for c in text:
        if state is None:  # default state, outside of special entity
            current += c
            if c in '"\'':
                # quoted string
                state = c
            elif c == '-':
                # probably "--" comment
                state = '-'
            elif c == '/':
                # probably '/*' comment
                state = '/'
            elif c == ';':
                # remove it from the statement
                current = current[:-1].strip()
                # and save current stmt unless empty
                if current:
                    yield current
                current = ''
        elif state == '-':
            if c != '-':
                # not a comment
                state = None
                current += c
                continue
            # remove first minus
            current = current[:-1]
            # comment until end of line
            state = '--'
        elif state == '--':
            if c == '\n':
                # end of comment
                # and we do include this newline
                current += c
                state = None
            # else just ignore
        elif state == '/':
            if c != '*':
                state = None
                current += c
                continue
            # remove starting slash
            current = current[:-1]
            # multiline comment
            state = '/*'
        elif state == '/*':
            if c == '*':
                # probably end of comment
                state = '/**'
        elif state == '/**':
            if c == '/':
                state = None
            else:
                # not an end
                state = '/*'
        elif state[0] in '"\'':
            current += c
            if state.endswith('\\'):
                # prev was backslash, don't check for ender
                # just revert to regular state
                state = state[0]
                continue
            elif c == '\\':
                # don't check next char
                state += '\\'
                continue
            elif c == state[0]:
                # end of quoted string
                state = None
        else:
            raise Exception('Illegal state %s' % state)

    if current:
        current = current.rstrip(';').strip()
        if current:
            yield current

/*
 Sau khi đã có các bảng Reference,
 Ta sẽ Join các bảng lại với nhau để tạo ra bảng cuối cùng: location_uk
 1. Create table location_uk
 2. JOIN data from table (vmo_demo_test.public)
 3. INSERT INTO TABLE location_uk
 4. Foreign key for location_uk_table
 */
-------------------------------------------------------------------------------------------------------------------

-- ------------------------------------------------------location_uk-----------------------------------------------------------
-- DROP TABLE vmo_demo_test.create_table_from_public.location_uk_table;
-- DROP TABLE vmo_demo_test.public.chien;
-- 1.

-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.location_uk_table
--     (--id INTEGER PRIMARY KEY,
--      district_id INTEGER,
--      pcon_id INTEGER,
--      ladcd_id INTEGER,
--      lau1_id INTEGER,
--      lau2_id INTEGER,
--      nut1_id INTEGER,
--      nut2_id INTEGER,
--      nut3_id INTEGER
--     );


-- 2 + 3
-- INSERT INTO vmo_demo_test.create_table_from_public.location_uk_table(district_id, pcon_id, ladcd_id, lau1_id, lau2_id, nut1_id, nut2_id, nut3_id)
--     SELECT DISTINCT
--         pconp.district_code_id,
--         pconp.pcon_id,
--         la.ladcd_id,
--         lln.lau1_id, lln.lau2_id, lln.nut1_id, lln.nut2_id, lln.nut3_id
--
--     FROM
--         (SELECT DISTINCT
--         lln.lau1_id, lln.lau2_id, lln.nut1_id, lln.nut2_id, lln.nut3_id
--             FROM vmo_demo_test.public.lau2_lau1_nuts lln) AS lln
--     JOIN
--         (SELECT DISTINCT
--             la.ladcd_id, la.lau1_id
--         FROM vmo_demo_test.public.local_authority la) AS la
--         ON lln.lau1_id = la.lau1_id
--     JOIN
--         (SELECT DISTINCT
--             tds.ladcd_id, tds.district_code_id
--         FROM
--             vmo_demo_test.public.two_dot_seven tds
--         UNION
--         SELECT DISTINCT
--             ml.ladcd_id, ml.district_code_id
--         FROM
--             vmo_demo_test.public.msoa_lsoa ml
--             ) tdsml
--         ON la.ladcd_id = tdsml.ladcd_id
--     JOIN
--          (SELECT DISTINCT
--             pc.district_code_id, pc.pcon_id
--          FROM
--             vmo_demo_test.public.postcode pc
--          UNION
--          SELECT DISTINCT
--             onp.district_code_id, onp.pcon_id
--          FROM
--             vmo_demo_test.public.ons_postcode onp
--              ) pconp
--         ON pconp.district_code_id = tdsml.district_code_id;


-- ////////////////////////////////////////location_uk_table//////////////////////////////////////////////////////////////////////////
-- 4. Tạo foreign key cho table location_uk_table
-- ALTER TABLE vmo_demo_test.create_table_from_public.location_uk_table
--     ADD CONSTRAINT fk_lau1_location_uk_table
--     FOREIGN KEY (lau1_id)
--     REFERENCES vmo_demo_test.create_table_from_public.lau1_table(id),
--     ADD CONSTRAINT fk_lau2_location_uk_table
--     FOREIGN KEY (lau2_id)
--     REFERENCES vmo_demo_test.create_table_from_public.lau2_table(id),
--     ADD CONSTRAINT fk_nut1_location_uk_table
--     FOREIGN KEY (nut1_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut1_table(id),
--     ADD CONSTRAINT fk_nut2_location_uk_table
--     FOREIGN KEY (nut2_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut2_table(id),
--     ADD CONSTRAINT fk_nut3_location_uk_table
--     FOREIGN KEY (nut3_id)
--     REFERENCES vmo_demo_test.create_table_from_public.nut3_table(id),
--     ADD CONSTRAINT fk_district_code_location_uk_table
--     FOREIGN KEY (district_id)
--     REFERENCES vmo_demo_test.create_table_from_public.district_code_table(id),
--     ADD CONSTRAINT fk_pcon_location_uk_table
--     FOREIGN KEY (pcon_id)
--     REFERENCES vmo_demo_test.create_table_from_public.pcon_table(id),
--     ADD CONSTRAINT fk_ladcd_location_uk_table
--     FOREIGN KEY (ladcd_id)
--     REFERENCES vmo_demo_test.create_table_from_public.ladcd_table(id);
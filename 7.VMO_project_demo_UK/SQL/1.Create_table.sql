/*

 File này sẽ tiến hành:
 1. Tạo các bảng để reference: lau1-2, nut1-2-3, pcon, district_code, ladcd
    Tạo xong sẽ up data
 2. Tạo Primary key cho table reference
 3. Sau đó sẽ tạo index cho fields (id, code) của các bảng reference


 */


-------------------------------------------------------------------------------------------------------------------
-- 1.
-------------------------------------------------------------------------------------------------------------------
-- ------------------------------------------------------lau1-----------------------------------------------------------


-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.lau1_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.lau1_table (code, name)
--     SELECT
--         lln.lau117cd, lau117nm
--     FROM
--         vmo_demo_test.public.lau2_lau1_nuts lln
--     UNION
--     SELECT
--         la.lau118cd, la.lau118nm
--     FROM
--         vmo_demo_test.public.local_authority la;
--
-- ------------------------------------------------------lau2-----------------------------------------------------------
--;
-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.lau2_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.lau2_table (code, name)
--     SELECT DISTINCT lau217cd, lau217nm FROM vmo_demo_test.public.lau2_lau1_nuts;
--
-- ------------------------------------------------------nut1-----------------------------------------------------------
--
-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.nut1_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.nut1_table (code, name)
--     SELECT
--         lln.nuts118cd, lln.nuts118nm
--     FROM
--         vmo_demo_test.public.lau2_lau1_nuts lln
--     UNION
--     SELECT
--         la.nuts118cd, la.nuts118nm
--     FROM
--         vmo_demo_test.public.local_authority la;
--
-- ------------------------------------------------------nut2-----------------------------------------------------------
--
-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.nut2_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.nut2_table (code, name)
--     SELECT
--         lln.nuts218cd, lln.nuts218nm
--     FROM
--         vmo_demo_test.public.lau2_lau1_nuts lln
--     UNION
--     SELECT
--         la.nuts218cd, la.nuts218nm
--     FROM
--         vmo_demo_test.public.local_authority la;
--
-- ------------------------------------------------------nut3-----------------------------------------------------------
--
-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.nut3_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.nut3_table (code, name)
--     SELECT
--         lln.nuts318cd, lln.nuts318nm
--     FROM
--         vmo_demo_test.public.lau2_lau1_nuts lln
--     UNION
--     SELECT
--         la.nuts318cd, la.nuts318nm
--     FROM
--         vmo_demo_test.public.local_authority la;
--
-- ------------------------------------------------------ladcd-----------------------------------------------------------
--
-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.ladcd_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.ladcd_table (code, name)
--     SELECT
--         la.lad18cd AS ladcd,
--         la.lad18nm AS ladnm
--     FROM
--         vmo_demo_test.public.local_authority la
--     UNION
--     SELECT
--         tds.lad11cd, tds.lad11nm
--     FROM
--         vmo_demo_test.public.two_dot_seven tds
--     UNION
--     SELECT
--         ml.ladcd, ml.ladnm
--     FROM
--         vmo_demo_test.public.msoa_lsoa ml
--     WHERE ladcd IS NOT NULL;
--
-- ------------------------------------------------------District_code-----------------------------------------------------------
--
-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.district_code_table
--     ( code VARCHAR,
--       name VARCHAR
--     );
-- INSERT INTO vmo_demo_test.create_table_from_public.district_code_table (code, name)
--     SELECT DISTINCT
--         SUBSTR(onp.pcds, 1, POSITION(' ' IN onp.pcds)) AS code,
--         SUBSTR(onp.pcds, 1, POSITION(' ' IN onp.pcds)) AS name
--     FROM (SELECT onp.pcds AS pcds
--           FROM vmo_demo_test.public.ons_postcode onp
--           UNION
--           SELECT pc.pcds
--           FROM vmo_demo_test.public.postcode pc
--           UNION
--           SELECT tds.pcds
--           FROM vmo_demo_test.public.two_dot_seven tds
--          ) AS onp
--     ORDER BY code;

-- ------------------------------------------------------pcon-----------------------------------------------------------

-- CREATE TABLE IF NOT EXISTS vmo_demo_test.create_table_from_public.pcon_table
--     ( code VARCHAR,
--       name VARCHAR
--     );

-- INSERT INTO vmo_demo_test.create_table_from_public.pcon_table(code, name)
--     SELECT
--         onp.pcon AS c,
--         onp.pcon AS n
--     FROM
--         vmo_demo_test.public.ons_postcode onp
--     WHERE onp.pcon IS NOT NULL
--     UNION
--     SELECT
--         pc.pcon,
--         pc.pcon
--     FROM
--         vmo_demo_test.public.postcode pc
--     WHERE pc.pcon IS NOT NULL
--     ORDER BY c;

-------------------------------------------------------------------------------------------------------------------
-- 2.
-------------------------------------------------------------------------------------------------------------------

-- ALTER TABLE vmo_demo_test.create_table_from_public.lau1_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.lau2_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.nut1_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.nut2_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.nut3_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.ladcd_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.district_code_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.pcon_table ADD COLUMN id SERIAL PRIMARY KEY;
-- ALTER TABLE vmo_demo_test.create_table_from_public.location_uk_table ADD COLUMN id SERIAL PRIMARY KEY;

-------------------------------------------------------------------------------------------------------------------
-- 3.
-------------------------------------------------------------------------------------------------------------------

-- CREATE INDEX idx_code_lau1_table
-- ON vmo_demo_test.create_table_from_public.lau1_table(code);
-- CREATE INDEX idx_id_lau1_table
-- ON vmo_demo_test.create_table_from_public.lau1_table(id);

-- CREATE INDEX idx_code_lau2_table
-- ON vmo_demo_test.create_table_from_public.lau2_table(code);
-- CREATE INDEX idx_id_lau2_table
-- ON vmo_demo_test.create_table_from_public.lau2_table(id);

-- CREATE INDEX idx_code_nut1_table
-- ON vmo_demo_test.create_table_from_public.nut1_table(code);
-- CREATE INDEX idx_id_nut1_table
-- ON vmo_demo_test.create_table_from_public.nut1_table(id);

-- CREATE INDEX idx_code_nut2_table
-- ON vmo_demo_test.create_table_from_public.nut2_table(code);
-- CREATE INDEX idx_id_nut2_table
-- ON vmo_demo_test.create_table_from_public.nut2_table(id);

-- CREATE INDEX idx_code_nut3_table
-- ON vmo_demo_test.create_table_from_public.nut3_table(code);
-- CREATE INDEX idx_id_nut3_table
-- ON vmo_demo_test.create_table_from_public.nut3_table(id);

-- CREATE INDEX idx_code_district_code_table
-- ON vmo_demo_test.create_table_from_public.district_code_table(code);
-- CREATE INDEX idx_id_district_code_table
-- ON vmo_demo_test.create_table_from_public.district_code_table(id);

-- CREATE INDEX idx_code_pcon_table
-- ON vmo_demo_test.create_table_from_public.pcon_table(code);
-- CREATE INDEX idx_id_pcon_table
-- ON vmo_demo_test.create_table_from_public.lau1_table(id);



